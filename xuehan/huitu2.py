#!/usr/bin/python
#-*- coding:utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

'''
labels='frogs','hogs','dogs','logs'
sizes=15,20,45,10
colors='yellowgreen','gold','lightskyblue','lightcoral'
explode=0,0.1,0,0
plt.pie(sizes,
        explode=explode,
        labels=labels,
        colors=colors,
        autopct='%1.1f%%',
        shadow=True,
        startangle=50)
plt.axis('equal')
plt.show()
'''
def draw(robot_num, robot_silent, robot_move, image_type):
	plt.figure()
	#plt.scatter(robot_num, robot_silent, marker="+", color="m", label="Robot silent")
	#plt.scatter(robot_num, robot_move, marker="x", color="r", label = "Run move")
	plt.plot(robot_num, robot_silent, marker="+", color="b", label="Robots keep static", linewidth=3)
	plt.plot(robot_num, robot_move, marker="x", color="r", label = "Robots are moving", linewidth=3)
	plt.xlabel("Robot number")
	plt.ylabel("Event counts")
	#plt.legend(loc = 'upper right')
	plt.legend(bbox_to_anchor=(0.40, 0.27),fontsize=20, loc=2, borderaxespad=0.)
	#plt.show()
	plt.savefig(image_type)


if __name__ == '__main__':

	image_type = ['cycles','instructions','cache-misses','cache-references','branch-misses','branch-instructions','mem-loads','dTLB-load-misses','cache-miss-rate']

	origin_file='./result_origin'
	origin_data = open(origin_file,'r').read().split('\n')[:-1]

	silent=[]
	move=[]
	for i in range(len(origin_data)):
		origin_data[i]=origin_data[i].split(':')
		tempdata = origin_data[i][2].split('/')
		for j in range(len(tempdata)):
			tempdata[j]=int(tempdata[j])
		tempdata.append( tempdata[2]*1.0/tempdata[3])

		if origin_data[i][1]=='0':
			silent.append(tempdata)
		else:
			move.append(tempdata)

	robot_num = [0,10,20,30,40,50,60,70,80,90,100,150,200,300]
	for i in image_type:
		temp_silent=[]
		temp_move=[]
		for j in silent:
			temp_silent.append( j[image_type.index(i)])
		for j in move:
			temp_move.append( j[image_type.index(i)])
		draw(robot_num, temp_silent, temp_move, i)

#	robot_num = [10,20,30,40,50,60,70,80,90,100,150,200,300]
#	#robot_num = [10,20,30,40,50,60,70,80,90,100,150,200]
#	time_no_ui = [2.1, 4.5, 8.1, 13.4, 21.3, 30.8, 47.4, 55, 68.6, 83.6, 197.6, 344,715]
#	#time_no_ui = [2.1, 4.5, 8.1, 13.4, 21.3, 30.8, 47.4, 55, 68.6, 83.6, 197.6, 344]
#	time_with_ui = [2.1, 5.1, 14.6, 21, 32.3, 47.8, 57.3,76.8,89.7,128, 260.5,452.5,1143]
#	#time_with_ui = [2.1, 5.1, 14.6, 21, 32.3, 47.8, 57.3,76.8,89.7,128, 260.5,452.5]








