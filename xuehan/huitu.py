#!/usr/bin/python
#-*- coding:utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

'''
labels='frogs','hogs','dogs','logs'
sizes=15,20,45,10
colors='yellowgreen','gold','lightskyblue','lightcoral'
explode=0,0.1,0,0
plt.pie(sizes,
        explode=explode,
        labels=labels,
        colors=colors,
        autopct='%1.1f%%',
        shadow=True,
        startangle=50)
plt.axis('equal')
plt.show()
'''

robot_num = [10,20,30,40,50,60,70,80,90,100,150,200,300]
#robot_num = [10,20,30,40,50,60,70,80,90,100,150,200]
time_no_ui = [2.1, 4.5, 8.1, 13.4, 21.3, 30.8, 47.4, 55, 68.6, 83.6, 197.6, 344,715]
#time_no_ui = [2.1, 4.5, 8.1, 13.4, 21.3, 30.8, 47.4, 55, 68.6, 83.6, 197.6, 344]
time_with_ui = [2.1, 5.1, 14.6, 21, 32.3, 47.8, 57.3,76.8,89.7,128, 260.5,452.5,1143]
#time_with_ui = [2.1, 5.1, 14.6, 21, 32.3, 47.8, 57.3,76.8,89.7,128, 260.5,452.5]

plt.figure()
plt.scatter(robot_num, time_no_ui, marker="+", color="m", label="Run time without ui")
plt.scatter(robot_num, time_with_ui, marker="x", color="r", label = "Run time with ui")
plt.xlabel("Robot number")
plt.ylabel("Run time(s)")
#plt.legend(loc = 'upper right')
plt.legend(bbox_to_anchor=(0.65, 0.17), loc=2, borderaxespad=0.)
#plt.show()
plt.savefig("time.jpg")







