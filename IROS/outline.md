

# Massively multi-robot simulation in gazebo

## Abstract
模拟器在机器人研究领域扮演着非常重要的角色，而Gazebo是其中应用最广泛的其中一个。Gazebo是用C/C++实现的3D动态机器人模拟器，它能够对真实世界的许多物理属性进行模拟，然而功能强大必然会需要更多的计算资源。针对gazebo的性能问题，在本文中我们设计了一个简单的benchmark对gazebo的scalability进行了评测，结果和我们预想的一样：随着机器人数量的增加，gazebo执行相同任务的时间变长，数量和时间呈二次多项式关系。实验结果表明目前gazebo并不适合进行大规模模拟。我们使用perf对gazebo分析进行分析，挖掘出gazebo的瓶颈为CPU的使用。最后我们对gazebo的性能优化提出了有效的建议。

## INTRODUCTION
多机器人系统与swarm-robots目前是机器人研究领域的重要研究方向，然而我们都知道，在该领域进行研究中如果都使用实体机器人那么成本将会是一个很大的问题，而且也根本没有必要。于是，模拟器就成了我们很好的选择。目前有很多流行的模拟器，例如：Stage/Player, Gazebo, ODE, Bullet, V-rep ，在文章[3]提到，有几大因素影响甚至决定用户挑选使用模拟器，这其中最重要的有：1. 模拟的真实度；2.是否开源；3. 模拟和真实场景的代码是否相同。Gazebo因其满足以上几个重要特性，所以它的使用率和普及程度是最高的.

Gazebo功能强大并且具有以上的优良特性，然而它也有自身的先天不足：因其固有的架构设计，Gazebo在大规模机器人模拟时性能会变得很差. 不像Stage这样的2D机器人模拟器用一个"点"来替代完整的机器人模型，可以模拟超大规模的多机器人系统(可以模拟10万个Poineer 3D robot的建议模型)[massive stage模拟]，Gazebo会随着模拟的机器人的数目增加performance会明显降低，模拟数量远远达不到那些2D模拟器所能达到的模拟数目。 但是机器人的数目和performance的量化关系具体是怎样的，我们发现很少有人进行这样的探索工作。那么就有一个问题，如果想要通过Gazebo进行多机器人模拟时（不一定是非常大的规模），那么到达多少规模时Gazebo的性能会下降到我们无法忍受？如果我们有一个量化的规律可以遵循，那么我们就可以提前预估我们要模拟的规模或者决定要不要使用Gazebo来进行模拟。

Gazebo的性能问题对于机器人领域的研究人员来讲并不陌生，但是除了模拟器的设计与实现人员可能并没有太多人去挖掘究竟是什么导致Gazebo的性能在机器人数量增加到一定规模后性能会变得很差。从计算机系统的角度分析可能有很多种原因：固有的计算量增加、频繁缺页、访存数量过多、多核利用率低、cache-misses骤升等等。针对以上问题，我们工作主要如下：

A.Contributions

首先，我们设计并实现了一个简单易用的benchmark，这其中包括一种两轮机器人。在该benchmark中，我们使用ROS程序来控制机器人的移动，通过修改参数来scale机器人的数目，并且实现了dispersion策略。利用该benchmark我们从多个维度对gazebo的性能进行了详细的评测。通过实验我们得出gazebo的性能和机器人的数目的量化关系。

我们站在计算机系统研究人员的角度，使用`perf_event`程序性能评测工具，对Gazebo在多种场景下进行了测试，分析了所有可能导致Gazebo性能随机器人规模增加而降低的原因，这其中包括CPU的负载、访存数量、cache 缺失率、缺页中断数目等等，最后我们发现导致gazebo性能减低的最根本原因是计算量的增加，使CPU的性能成为了性能的瓶颈。

## RELATED WORK
 经过充分调研后，我们发现关于多机器人在模拟器环境下进行大量模拟的量化性能评测的工作并不是很多。早在十几年前，[2004 Dorigo]、[2005,$̧ahin]就对swarm robotics的特点和目标的研究进行了调研，虽然年代久远，但是它们仍有很好的借鉴意义。最近几年也有关于多机器人模拟器的调研的研究，Serena Ivaldi[3] 基于用户的反馈非常详细地调研了动态模拟器的使用率、应用场景、仿真度、计算负载、模拟速度、稳定性、模拟精度等特性。他们调研的范围很广：基本覆盖了大多数目前在使用的模拟器（eg. Gazebo,ODE, Bullet, V-rep,Webots）. 文章[4]将对比了两大类模拟器：商业模拟器和开源免费模拟器。他们对目前流行的模拟器进行了大体上的比较，最后他们设计了一个实验来展现在模拟器和实际场景下运行时控制代码的差异，他们工作的目的是帮助beginner选择最适合他们的模拟器。我们没有足够的空间来列出一个survey，但是下面介绍的ROS和Gazebo的还是需要读者了解。

- ROS:Robot Operating System (ROS)并非传统意义上的操作系统，它只是提供了类似于操作系统功能的工具集[5]。ROS基于topic、service、nodes等构建：node是一个运行的进程或者程序模块，它们可以分布在不同的机器或虚拟机器上通过TCP/IP, UDP网络协议进行通信；service和topic为不同类型的信息，node进行信息通信的对象就是service或topic，`roscore`节点时刻对service和topic进行追踪和管理。ROS对机器人的底层结构进行抽象，保证了代码的重用性。另外，ROS支持C++,python，java等多种语言，本文的benchmark程序就是由C++编写的ROS程序。

- Gazebo: Gazebo是从Player Project中衍生出的3D动态模拟器[6],目前支持ODE、Bullet、Simbody、DART四种动态物理引擎。使用OGRE[7],Gazebo提供逼真的环境渲染，包括高质量的照明，阴影和纹理。Gazebo可以从激光测距仪，2D/3D摄像机，Kinect风格的传感器，接触式传感器，力矩等等生成传感器数据并且在获取数据时可以选择性地引入噪音。因其强大的功能、高保真度、开源以及能够和ROS、Player无缝对接等优点目前已经变得非常流行。但是，当机器人规模变大，它的运行速度会变得很慢[8],我们在本文中对其performance进行了评测。 

此外，有一些工作是直接面向模拟器或模拟器所基于的动态引擎的速度、精度的评测。Richard Vaughan在[12]中以Player作为controller，详细评测了Stage在不同规模的Pioneer Robot情况下的performance。Tom Erez等在文章[9]中引入了一种`speed-accuracy`的评测方法对Bullet、ODE、Havok等引擎进行了评测。Steven Peters在文章[10]、[11]对Gazebo进行了评测，他们主要的关注是Gazebo在使用四种不同引擎时的性能和精度的差异,但是他们并没有解释实验结果的原因。Hai Yang在[13]借鉴了计算机系统分析的方法，使用Vtune[14],针对Gazebo在多核CPU下运行时CPU的行为进行跟踪监测，分析了Gazebo的热点函数。

## BENCHMARK
为了获取Gazebo的运行时数据，我们需要一个benchmark，但是很少有关于gazebo的benchmakrk的研究. 我们的目的是评测Gazebo在swarm场景下的实时性能表现，并不过多的关注单个机器人的复杂度对Gazebo性能的影响，所以我们就想使用一种非常简单的机器人模型来作为我们的评测用例。为了设计我们的benchmark，我们在偶然的机会中找到了项目[15],因其使用的是MIT协议，可以进行修改然后重新发布,所以我们借鉴了他们的工作，对其进行了一定修改后来作为我们的benchmark。在本文的工作中，我们采用一种两轮机器人模型，通过编写ROS的节点程序来控制我们的机器人的移动，通过记录Gazebo完成一项模拟任务的时间来作为性能的评价标准。下面我们针对机器人模型和控制器做详细的介绍。

### Robot models 

为了能模拟更大规模的机器人，我们使用更简单的机器人模型。Benchmark提供的机器人模型为一种两轮机器人，有三个重要部件`body_link`,`left_wheel`, `right_wheel`。左右两个轮子的半径为0015长度为0.004的圆柱体，中间为是一个正方形的盒子，轮子和盒子中间通过一个关节连接，该关节为两个轮子提供滚动的动力。机器人人的外观颜色对实验没有任何影响，因此我们在设置颜色时专门挑选了比较醒目的颜色。figure 1 展示了我们的机器人urdf的结构和在gazebo的样子。

<img src="./robot_model.png" title="model" width="400" />
<img src="./robot.png" width="400">

### robot controller
任何一个单独的机器人控制器都不太可能满足所有的评测场景，我们在设计机器人控制器的时候也不必刻意追求满足所有的场景。在实现一个机器人控制器时，我们应该满足一些具有代表性的场景即可，此外，要设计的尽量简单，尽可能的减少计算资源的使用，从来减少机器人控制器本身运行对gazebo评测的性能影响。
机器人部署是一个典型的多机器人系统问题，具有实用性和代表性[17]。我们的控制器实现分散的部署策略，让众多的机器人在空闲的空间中分散开来,最终使得所有的机器人之间的间隔都大于一个阈值。这是一种非常简单的策略，然而非常适用于Gazebo这样基于网格的模拟器的性能评测。

在实现该策略的过程中我们使用了一种弹簧模型来确定机器人的移动方向和运动速度。假设我们已经得到该机器人和它m个邻居的所有坐标（由x和y两个值确定），那么每个邻居机器人到该机器人的坐标都会形成一个二维向量V，将所有向量相加之后得到的向量方向即为该机器人的运动方向，然后将这个向量的长度乘以一个相应的系数，那么得到的即为该机器人的速度。该过程在figure 2中被展示。

**插入公式**

<img src="./tanhuang.png" width="300">

实际上，并不是直接对机器人施加一个速度，而是分别对两个轮子进行计算后得到一个反馈速度，因为篇幅原因，我们不在此进行详细讲解
下面我们将具体介绍控制器的算法。
```
1. 获取所有机器人的坐标，计算每两个机器人的间隔
2. 针对每个机器人，对其和其他机器人的距离进行排序，筛选出该机器人的距离最近的m个邻居机器人
3. 使用弹簧模型计算计算当前机器人下一轮运动的方向和速度
4. 判断每个所有机器人的间隔是否大于设置的阈值，如果都大于则停止，否则重复执行1-3
```
系统工程中，经常使用多个benchmark来测试多个方面的特性，而我们的benchmark可以作为未来benchmark集的重要组成部分。

## Experiment
为了测试Gazebo的性能变化，以及验证我们的benchmark是否有效，我们设计了相关实验来进行相关验证。我们通过在设置benchmark中机器人的不同数目，获取Gazebo完成该模拟任务所需要的时间，进而对数据结果进行分析，最终得到Gazebo性能和机器人数目的变化所对应的规律或相关函数。我们猜测图形渲染和用户界面会对Gazebo的性能产生明显的影响。为了验证图像显示对Gazebo性能的影响，我们在gzcient开启和关闭两种情况下分别进行了测试从而验证我们的猜想。
### environment
一般情况下，我们用Gazebo进行模拟时使用普通配置的机器即可，没有必要使用太高性能的机器。当然，如果需要模拟大量的机器人时那么也需要相应地提高机器的性能来满足Gazebo对计算资源的需求。
我们在实验的场景为一般场景，在该场景下不需要太多的计算资源，而且，这并不影响对数据的分析和拟合。具体的实验环境在table 3中详细展示。CPU为普通的i5处理器，内存和磁盘已经显卡都是常用的型号和规格。我们在选择Gazebo版本时没有选择最新版本的9.0.0而是选择了相对稳定的7.0版本，与此对应的ROS的版本的Kinetic，操作系统我们选择Ubuntu 16.04（为了兼容ROS）。

|   Item |  model or version | 
|:-------:|:-------------: |
|   Gazebo |  7.0.0  |
| ROS | Kinetic |
| OS | Ubuntu 16.04 (kernel version 4.13.0) |
| CPU | Intel(R) Core(TM) i5-3470 CPU @ 3.20GHz * 4 |
| memory | KVR16N11D6A/4-SP * 4 （16G）|
| Disk | ST500DM002-1BD142 (500G) | 
| GPU|Gallium 0.4 on AMD CAICOS|

### benchmark gazebo
我们实验从机器人数目和是否打开gui两个维度展开。我们在实验前分析dispersion策略以及Gazebo的发现可以大致猜测Gazebo的性能表现和机器人的数目应该大致呈二次多项式分布，并且，因为gzclient也需要相应的计算资源，因此我们自然而然可以猜测到打开gui也会给gazebo的性能带来一定的影响。

为了验证我们猜想，我们执行以下实验。
1. 首先设置benchmark的机器人数目的参数。
2. 启动benchmark，此时gzserver会一起跟着启动，并且会在gazebo中spawn出相应数目的机器人 （如果参数中设置启动gzclient，那么gzclient也会一起被启动）
3. 启动robot contorller，dispersion策略开始执行
4. 运行结束，记录运行时间
5. 设置不同机器人数目，重复上述的步骤

执行以上步骤之后我们会分别得到两组数据，一组为打开gui时benchmark的数据，另一组为关闭gui的数据，每个步骤重复10次后取平局值得到table 1的数据。

| robot number| run time with gui | run time with no gui | 
| :------:| :------:| :-------: |
|10|||
|20|||
|30|||
|40|||
|50|||
|...|....|....|

<img src="./time.jpg" title="Time" width="300" />

直接观察table 3的数据，我们可能很难直接发现规律，于是我们将数据做成散点图。通过观察散点图我们很容易发现，无论是在打开gui还是关闭gui的情况下，机器人数目和执行时间的关系和二次多项式的分布非常相似。然后我们通过Python的数据拟合工具进行拟合得到运行时间T和机器人数目x的关系(下标是1的表示打开gui，2则表示关闭gui)： 

    T1 = A1 * x^2 + B1 * x + C1  
    T2 = A2 * x^2 + B2 * x + C2  
其中A1 、B1、 C1的值分别为..., A2, B2, C的值为...,我们对上述两个函数做图（figure 3中的两条曲线），其拟合的效果非常好，从而验证了我们的机器人数目与运行时间的对应关系的猜想。

我们观察拟合出的两条曲线，可以很明显的发现，gui对gazebo的影响确实存在，而且随着机器人数目的增加，这种影响会越来越明显。因此，我们在用gazebo模拟时，可以适当关闭gui从而提高gazebo的性能。

### analyze with perf_event

上个章节我们总结出Gazebo的性能和机器人数目的关系，然而我们想往深处探索一下：为什么会出现这样的性能变化曲线或者说是什么原因导致gazebo的性能在多机器人的情况下变慢。为此，我们尝试使用性能评测工具`perf evnt`来记录gzserver的执行过程，并且对记录的结果进行分析，进而找出gazebo的性能和其中一些常见指标的关系。
我们在关闭gui的条件下，对gazebo检测。机器人的数目设置与上节实验一致，都是从10个机器人开始逐渐增加到300个。我们在不同机器人数目的场景下，使用perf依次对gzserver监控10s，然后记录监控数据。因为机器人数目不够并且轮子速度过快时运行时间会小于10s，为此我们统一缩小所有数目场景下benchmark的`velocity ratio`参数到0.05，保证每个场景的运行时间都大于10s。figure 4展示了我们的实验结果，memory-store和page-fault事件的次数都为0或1，因此没有在图表中体现。

<img src="./xuehan/cycles.png" tittle="Cycles" width="300">
<img src="./xuehan/instructions.png" tittle="instructions" width="300">
<img src="./xuehan/branch-misses.png" tittle="branch-miss" width="300">
<img src="./xuehan/branch-instructions.png" tittle="branch-instructions" width="300">
<img src="./xuehan/cache-misses.png" tittle="cache-miss" width="300">
<img src="./xuehan/cache-references.png" tittle="cache-references" width="300">
<img src="./xuehan/cache-miss-rate.png" tittle="cache-miss-rate" width="300">
<img src="./xuehan/mem-loads.png" tittle="branch-miss" width="300">

figure 4 的各项性能指标从多个方面程序的设计和运行情况：cpu cycles和instructions反映出程序对CPU负载的需求，即计算量的需求；cache-miss-rate和branch-misses指标分别反映出程序的空间局部性和空间局部性；memory-load和memory-store与instruction的比例则反映出程序的访存指令占比，从一定程度上反映出是否为io密集型程序。我们分析各项指标的变化趋势以及各项指标之间的关系，我们有以下发现：

1. gzserver在空跑的情况下，几乎不会占用计算资源
2. cache的miss率随机器人数目增加而升高,且相同机器人数目下，机器人运动时要比静止时的miss高很多
3. 访存比本身不高且没有随机器人数目增加而上升
4. 数量到30时，CPU的使用率就几乎到达了最高，数目再增加时一直保持在CPU可用的最高水平
5. 分支预测miss率没有随机器人数目变化而发生明显变化
6. 总体来看gazebo模拟多机器人时，机器人运动与否对性能指标的影响不大

针对以上发现，我们可以得出以下结论：gazebo的性能瓶颈是宿主机的CPU，从cycle和instruction指标可以看出，当机器人数目到达30个左右时CPU基本上已经处于满负载状态，虽然cache的miss率在不断上升，它可能成为一个潜在的瓶颈，但是它的影响相对于CPU的负载对gazebo的影响来讲是可以忽略的。因此从当前实验环境和数据来看，最大的瓶颈仍然是CPU的计算能力，而当CPU的计算能力足够强时，cache miss率可能会成为瓶颈。

CPU的计算能力是gazebo的性能瓶颈，perf进一步帮助我们找到哪些函数消耗了大量的计算资源。我们经过分析perf记录的数据发现，computeRow是需要重点关注的对象，该函数用来解决Linear Complementarity Problem(LCP) in iterative projected Gauss-Seidel Solver. 大量的计算过程都在这个函数中。table 3列举了一些机器人运动时的场景下，computeRow函数对CPU的使用情况。computeRow的调用关系在figure4中展示，gazebo在更新物理引擎时会频繁调用该函数,我们想提升gazebo的性能，那么我们应该重点关注这个函数和调用它的函数。 

|数目 | task-clock(%) | cycles(%)| instructions (%)|
|:--:| :--: | :---: | :-----:|
|20|55.63|56.88|81.09|
|30|51.47|52.67|78.34|
|150| 46.72|47.55|78.31|

<img src="./computeRow.png" width="300">

## CONCLUSION AND FUTURE WORK
在本文中，我们关注Gazebo在多机器人场景下的性能分析。我们都知道，Gazebo的性能是随着机器人的数量而下降的，然而这种性能变化的规律大家都只是一个感性的认识并不知道其具体变化的量化规律。而且，不像Stage/Player等其他模拟器，Gazebo并没有一个通用意义上性能benchmark。
为了解决以上问题，我们设计了一个全新的benchmark。我们使用该benchmark完成了对Gazebo在多场景下的测试，通过对测试数据的分析，我们拟合出机器人的数目和Gazebo的性能的规律（函数）,量化分析了Gazebo的性能表现。
另外，我们针对Gazebo在不同机器人数目的多场景下，使用linux的perf_event工具对其Gazebo的各项系统指标进行了统计。通过对这些指标的分析，最终我们确定Gazebo的性能瓶颈为其计算量的增加。
目前我们只分析了Gazebo在使用ODE引擎时的性能表现，另外三个引擎的性能也需要在未来的工作中被测试。此外，我们的benchmark应用分散策略来实现评测，在后续工作中可以增加其他的测试策略。
Gazebo的性能瓶颈需要解决，结合以上章节的分析结果，我们认为有以下建议来提升Gazebo的可拓展性：
1. 分布式
Gazebo的系统架构是“分布式的”[16]，然而并不是真正意义上的分布式。例如计算量相对比较大的`Physics Library`只是放在一个计算节点上，并不能分布到多个机器上来充当一个`Physics Library`角色。实现分布式，充分利用多台机器的计算资源是最有效的提升可Gazebo拓展性的方法。
2. 局部优化 
以上实验数据显示，ComputeRow函数占用了大量（将近一半）的计算资源。如果能减少该函数消耗的计算资源，或者降低该函数的调用频率，那么gzserver的运行效率将会大幅度的提升。

我们分析gazebo的源码发现，Gazebo对并行化进行了很全面的优化，支持opencl、cuda和openmp等计算架构。因此，从用户的角度来考虑，如果想要gazebo拥有更好的性能，可以择更多的核的CPU和性能更强的GPU。

## REFFERENCE

1. Dorigo, M., & S ̧ahin, E. (Eds.) Special issue: swarm robotics. Autonomous Robots, 17(2–3), 2004.

2.  ̧ahin5, E., & Spears, W. (2005). In Lecture notes in computer science: Vol. 3342. Swarm robotics: SAB 2004 international workshop, Revised Selected Papers Santa Monica, CA, USA, July 17, 2004. Heidelberg: Springer.

3. Tools for dynamics simulation of robots: a survey based on user feedback.
4. A Survey and Comparison of Commercial and Open-Source Robotic Simulator Software
5. http://wiki.ros.org/ROS/Introduction
6. http://www.gazebosim.org/
7. https://www.ogre3d.org/
8. Design and use paradigms for gazebo, an open-source multi-robot sim-ulator.
9. Simulation Tools for Model-Based Robotics: Comparison of Bullet, Havok, MuJoCo, ODE and PhysX
10. Simple benchmarks for speed and accuracy of rigid body dynamic simulators。ECCOMAS Multibody 2015
11. Comparison of Rigid Body Dynamic Simulators for Robotic Simulation in Gazebo.ROSCon 2014 .
12. Massively multi-robot simulation in stage 
13. A Case Study on the Performance of Gazebo with Multi-core CPUs
14. https://software.intel.com/en-us/intel-vtune-amplifier-xe/
15. https://github.com/yangliu28/swarm_robot_ros_sim
16. http://gazebosim.org/tutorials?tut=architecture#Introduction 
17. Howard, A., Parker, L. E., & Sukhatme, G. S. (2006). Experiments with large heterogeneous mobile robot team: exploration, mapping, deployment and detection. International Journal of Robotics Research, 25(5), 431–447.